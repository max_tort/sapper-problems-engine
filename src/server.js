import sirv from 'sirv';
// import polka from 'polka';
import compression from 'compression';
import * as sapper from '@sapper/server';
import express from 'express'
import {
  getFullErrorFlag,
  isAvailableEmail,
  authorize,
  login
} from './util/utilities'
import fetch from 'node-fetch'
import cookieParser from 'cookie-parser'
import path from 'path'

// using .env
import dotenv from 'dotenv'
dotenv.config()

const {
  PORT,
  NODE_ENV
} = process.env;
const dev = NODE_ENV === 'development';

const app = express()
app.use(express.json()) // for parsing application/json
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.use(cookieParser()) // for parsing cookies
app.use(sirv('static', {
  dev
}))

const apiKey = process.env.GOOGLE_API_KEY || 'none'

app.get('/googleApiKey', (req, res) => {
  // console.log('googleApiKey');
  let result = process.env.GOOGLE_API_KEY || false
  // console.log(result);
  res.json({result})
})

app.post('/findPlace', async (req, res) => {
  let countryCode = req.body.countryCode || false
  let {input, inputtype, types} = req.body
  // const queryURL = `https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${input}&inputtype=${inputtype}&type=${type}&key=${apiKey}&fields=formatted_address,name`
  const queryURL = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${input}&key=${apiKey}&language=ru-RU&types=${types}`
  try {
    // console.log(queryURL);
    let response = await fetch(queryURL + (countryCode ? `&components=country:${countryCode}` : ''))
    let result = await response.json()
    if (countryCode) {
      result.predictions = result.predictions.filter(e => e.types.includes('school'))
    }
    res.send(result)
  }
  catch (e) {
    res.sendStatus(400)
    console.log(e);
  }
})

app.post('/getLocalityDetails', async (req, res) => {
  const { place_id } = req.body
  const fields = 'address_components'
  const queryURL = `https://maps.googleapis.com/maps/api/place/details/json?key=${apiKey}&place_id=${place_id}&fields=${fields}&language=ru-RU`
  try {
    // console.log(queryURL)
    let response = await fetch(queryURL)
    let result = await response.json()
    const address_components = result.result.address_components
    let locality = address_components[0].long_name
    let country = address_components[address_components.length - 1].long_name
    let countryCode = address_components[address_components.length - 1].short_name.toLowerCase()
    res.send({
      locality,
      country,
      countryCode
    })
  }
  catch (e) {
    res.sendStatus(400)
    console.log(e);
  }
})

app.post('/register/validate', async (req, res, next) => {
  const values = req.body
  let errorFlags = {}

  await Promise.all(
    Object.keys(values)
    .map(
      async key => {
        errorFlags[key] = await getFullErrorFlag(key, values[key])
      }
    )
  )
  // Object.keys(values).forEach(key => {
  //   errorFlags[key] = await getFullErrorFlag(key, values[key])
  // })
  res.json(errorFlags)
})

app.post('/auth/login', login)

app.get('/auth/logout', (req, res, next) => {
  res.cookie('balatech-authorization', {maxAge: 0})
  res.redirect('/register')
})

app.get('/problems', authorize)
app.get('/stats', authorize)
app.get('/contests', authorize)
app.get('/', authorize)

app.get('/engine', authorize, (req, res) => {
  res.sendFile(path.join(__dirname, '../../../static/game', 'index.html'))
})

app.use(
  compression({
    threshold: 0
  }),
  sapper.middleware({
    session: (req, res) => ({
      jwt: req.cookies['balatech-authorization'] || 'none',
      PROBLEMS_API_HOST: process.env.PROBLEMS_API_HOST
    })
  })
)

app.listen(PORT, err => {
  if (err) console.log('error', err);
});