import fetch from 'node-fetch'
import validator from 'validator'

const cookieKey = "balatech-authorization"

async function isAvailableEmail(email) {
  if (process.env.PROBLEMS_API_HOST === undefined)
    throw 'PROBLEMS_API_HOST environment variable is not set. check .env file'
  if (email === undefined)
    throw '"email" argument is not defined'
  let host = process.env.PROBLEMS_API_HOST
  try {
    let res = await fetch(`${host}/auth/isAvailableEmail`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accepts': 'application/json'
      },
      body: JSON.stringify({email: email})
    })
    return (await res.json()).result
  }
  catch (err) {
    console.log(err);
  }
}

const isPassword = password => (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(password))
const isName = name => (/^[A-я\s]+$/gu.test(name))
const isEmail = email => (validator.isEmail(email))

async function getFullErrorFlag(type, value) {
  if (value.length === 0) {
    return 'errorRequired'
  }
  let flag = false
  switch (type) {
    case 'firstName':
    case 'lastName':
      flag = !isName(value) ? 'errorOnlyLetters' : false
      break
    case 'password':
      flag = !isPassword(value) ? 'errorPaswordShould' : false
      break
    case 'email':
      flag = !(await isAvailableEmail(value)) ? 'errorEmailExists' : false
      break
  }
  return flag
}

async function login(req, res, next) {
  const host = process.env.PROBLEMS_API_HOST || 'none'
  try {
    let { email, password } = req.body
    if (!email || !password) {
      res.sendStatus(400)
      return
    }
    let response = await fetch(`${host}/auth/login`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'Accepts': 'text/plain',
      },
      body: JSON.stringify({email, password})
    })
    let jwtCookie = await response.textConverted()
    // console.log(jwtCookie);
    res.cookie(cookieKey, jwtCookie)
    // res.sendStatus(200)
    res.redirect('/')
    return
  }
  catch (err) {
    console.log(err);
    res.redirect('/login')
    return
  }
}

async function authorize(req, res, next) {
  const host = process.env.PROBLEMS_API_HOST || 'none'
  const errorPath = '/register'
  let jwtCookie = req.cookies[cookieKey]
  if (jwtCookie) {
    // console.log({jwtCookie});
    try {
      let response = await fetch(`${host}/auth/refresh`, {
        headers: {
          'authorization': jwtCookie
        }
      })
      if (response.status === 200) {
        let newJWT = await response.text()
        // console.log(newJWT)
        res.cookie(cookieKey, newJWT)
        next()
      }
      else
        res.redirect(errorPath)
    }
    catch (err) {
      res.redirect(errorPath)
    }
  } 
  else {
    res.redirect(errorPath)
  }
}

export {
  login,
  authorize,
  isPassword,
  isName,
  getFullErrorFlag,
  isAvailableEmail,
  isEmail
}